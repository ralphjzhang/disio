include_directories(${PROJECT_SOURCE_DIR}/disio)
link_directories(${PROJECT_BINARY_DIR}/disio)

add_executable(sync_server sync_server.cpp)
add_executable(sync_client sync_client.cpp)
add_executable(async_server async_server.cpp)

target_link_libraries(sync_server boost_thread boost_system pthread)
target_link_libraries(sync_client boost_system pthread)
target_link_libraries(async_server boost_system pthread)

