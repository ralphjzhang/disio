cmake_minimum_required(VERSION 2.8)
project(DISIO)

#find_package(Boost) I guess this only works if we have boost installed as system libs

set(CMAKE_CXX_FLAGS "--std=c++0x -Wall -Wextra")

include_directories(${PROJECT_SOURCE_DIR}/3rd-party/include)
link_directories(${PROJECT_SOURCE_DIR}/3rd-party/lib)

add_subdirectory(disio)
add_subdirectory(test)
add_subdirectory(perf)
add_subdirectory(example)

